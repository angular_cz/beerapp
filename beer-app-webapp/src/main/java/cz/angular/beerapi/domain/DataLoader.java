package cz.angular.beerapi.domain;

import cz.angular.beerapi.domain.brewery.Brewery;
import cz.angular.beerapi.domain.brewery.BreweryFacade;
import cz.angular.beerapi.domain.brewery.BreweryRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by vita on 15.12.14.
 */
@Component
public class DataLoader {

    private static Logger logger = Logger.getLogger(DataLoader.class);

    @Autowired
    BeerRepository beerRepository;

    @Autowired
    BreweryRepository breweryRepository;

    @Autowired
    BreweryFacade breweryFacade;

    @PostConstruct
    public void initialize() {

        List<Brewery> breweries = new ArrayList<Brewery>();

        breweries.add(new Brewery("Benešov", 1569, "Benešov"));
        breweries.add(new Brewery("Bernard pivo", 1991, "Humpolec"));
        breweries.add(new Brewery("Beroun", 1998, "Beroun"));
        breweries.add(new Brewery("Braník", 1898, "Praha"));
        breweries.add(new Brewery("Broumov", 1994, "Broumov"));
        breweries.add(new Brewery("Brtník \"U Fleků\"", 1499, "Praha"));
        breweries.add(new Brewery("Budějovický Budvar", 1495, "České budějovice"));
        breweries.add(new Brewery("Černá Hora", 1298, "Černá hora"));
        breweries.add(new Brewery("Chmelař - Žatec", 1780, "Žatec"));
        breweries.add(new Brewery("Chodovar", 1573, "Chodová planá"));
        breweries.add(new Brewery("Eggenberg", 1560, "Český krumlov"));
        breweries.add(new Brewery("Excelent - Rýmařov", 1662, "Rýmařov"));
        breweries.add(new Brewery("Gerhard", 1573, "Brumov"));
        breweries.add(new Brewery("Herold Březnice", 1506, "Březnice"));
        breweries.add(new Brewery("Holba", 1874, "Hanušovice"));
        breweries.add(new Brewery("Hostan", 1363, "Znojmo"));
        breweries.add(new Brewery("Hradec Králové", 1844, "Hradec Králové"));
        breweries.add(new Brewery("HSP", 1326, "Hlučín"));
        breweries.add(new Brewery("Janáček - Uherský Brod", 1894, "Uherský Brod"));
        breweries.add(new Brewery("Karlovy Vary", 1879, "Karlovy vary"));
        breweries.add(new Brewery("Klášter", 1177, "Klášter Hradiště nad Jizerou"));
        breweries.add(new Brewery("Knížecí pivovar - Lanškroun", 1700, "Lanškroun"));
        breweries.add(new Brewery("Korbel - Malý Rohozec", 1850, "Malý Rohozec"));
        breweries.add(new Brewery("Krušovice", 1517, "Krušovice"));
        breweries.add(new Brewery("Kutná hora", 1573, "Kutná hora"));
        breweries.add(new Brewery("Litoměřice", 1720, "Litoměřice"));
        breweries.add(new Brewery("Litovel", 1770, "Litovel"));
        breweries.add(new Brewery("Louny", 1892, "Louny"));
        breweries.add(new Brewery("Kunc", 1994, "Hodonín"));
        breweries.add(new Brewery("Náchod", 1872, "Náchod"));
        breweries.add(new Brewery("Nošovice", 1970, "Nošovice"));
        breweries.add(new Brewery("Nová Paka", 1870, "Nová paka"));
        breweries.add(new Brewery("Novoměstský pivovar", 1993, "Praha"));
        breweries.add(new Brewery("Nymburk", 1785, "Nymburk"));
        breweries.add(new Brewery("Ostravar", 1897, "Ostrava"));
        breweries.add(new Brewery("Pardubice", 1871 ,"Pardubice"));
        breweries.add(new Brewery("Pegas", 1989 , "Brno"));
        breweries.add(new Brewery("Pelhřimov", 1552, "Pelhřimov"));
        breweries.add(new Brewery("Pivovarská bašta - Vrchlabí", 1997, "Vrchlabí"));
        breweries.add(new Brewery("Pivovarský dvůr Chýně", 1992, "Chýně"));
        breweries.add(new Brewery("Plzeňský Prazdroj", 1842, "Plzeň"));
        breweries.add(new Brewery("Podkováň", 1434, "Dolní cetno"));
        breweries.add(new Brewery("Polička",1517,"Polička"));
        breweries.add(new Brewery("Právovárečný pivovar", 1844, "Hradec králové"));
        breweries.add(new Brewery("Prostějov", 1897, "Prostějov"));
        breweries.add(new Brewery("Protovín", 1540, "Protivín"));
        breweries.add(new Brewery("První pražský měšťanský", 1895, "Praha"));
        breweries.add(new Brewery("Rakovník", 1454, "Rakovník"));
        breweries.add(new Brewery("Rebel", 1834, "Havlíčkův Brod"));
        breweries.add(new Brewery("Regent - Třeboň", 1379, "Třeboň"));
        breweries.add(new Brewery("Rožnov pod Radhoštěm", 1712, "Rožnov pod Radhoštěm"));
        breweries.add(new Brewery("Rychtář", 1913, "Hlinsko"));
        breweries.add(new Brewery("Samson", 1913, "České Budějovice"));
        breweries.add(new Brewery("Sedlec", 1470, "Most"));
        breweries.add(new Brewery("Starobrno", 1872, "Brno"));
        breweries.add(new Brewery("Staropramen", 1869, "Praha"));
        breweries.add(new Brewery("Strakonice", 1649, "Strakonice"));
        breweries.add(new Brewery("Svijany", 1564, "Svijany"));
        breweries.add(new Brewery("Svitavy", 1256, "Svitavy"));
        breweries.add(new Brewery("U Lípy - Svinišťany", 1991, "Dolany u Jaroměře"));
        breweries.add(new Brewery("Ústí nad Labem", 1795, "Ústí nad Labem"));
        breweries.add(new Brewery("U Vozků - Liberec", 1995, "Liberec"));
        breweries.add(new Brewery("Velké Březno", 1753, "Velké Březno"));
        breweries.add(new Brewery("Velké Popovice", 1870, "Velké Popovice"));
        breweries.add(new Brewery("Vratislavice nad Nisou", 1872, "Vratislavice nad Nisou"));
        breweries.add(new Brewery("Vsetín", 1663, "Vsetín"));
        breweries.add(new Brewery("Vyškov", 1680, "Vyškov"));
        breweries.add(new Brewery("Vysoký Chlumec", 1611, "Vysoký Chlumec"));
        breweries.add(new Brewery("Zlatovar - Opava", 1825, "Opava"));
        breweries.add(new Brewery("Zubr", 1872, "Přerov"));
        breweries.add(new Brewery("Zvíkovské Podhradí", 1994, "Záhoří u Písku"));

        for (Brewery brewery : breweries) {
            breweryFacade.save(brewery);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {


            }

        }

        List<BeerLoader> beers = new ArrayList<BeerLoader>();
        beers.add(new BeerLoader("Bakalář", "Nealko", "Rakovník", "světlé pivo"));
        beers.add(new BeerLoader("Bakalář", "8", "Rakovník", "světlé pivo"));
        beers.add(new BeerLoader("Bakalář Dia", "Dia", "Rakovník", "světlé pivo"));
        beers.add(new BeerLoader("Bakalář", "10", "Rakovník", "světlé pivo"));
        beers.add(new BeerLoader("Bakalář", "10", "Rakovník", "tmavé pivo"));
        beers.add(new BeerLoader("Bakalář", "12", "Rakovník", "světlé pivo"));
        beers.add(new BeerLoader("Benedikt", "10", "Louny", "bylinné pivo"));
        beers.add(new BeerLoader("Bernard", "10", "Bernard pivo", "světlé pivo"));
        beers.add(new BeerLoader("Bernard", "11", "Bernard pivo", "světlé pivo - pouze v sudech"));
        beers.add(new BeerLoader("Bernard", "11", "Bernard pivo", "polotmavé pivo"));
        beers.add(new BeerLoader("Bernard ležák", "12", "Bernard pivo", "světlý ležák pivo"));
        beers.add(new BeerLoader("Bernard sváteční", "12", "Bernard pivo", "sváteční ležák pivo"));
        beers.add(new BeerLoader("Berounský medvěd", "10", "Beroun", "světlé výčepní pivo"));
        beers.add(new BeerLoader("Berounský medvěd", "10", "Beroun", "polotmavé výčepní pivo"));
        beers.add(new BeerLoader("Berounský medvěd", "12", "Beroun", " ležák pivo"));
        beers.add(new BeerLoader("Berounský medvěd", "13", "Beroun", "tmavé speciální pivo"));
        beers.add(new BeerLoader("Blanický rytíř", "10", "Benešov", "světlé výčepní pivo"));
        beers.add(new BeerLoader("Bohém", "8", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Staročeské pivo Bohemia", "10", "Benešov", "světlé pivo"));
        beers.add(new BeerLoader("Staročeské pivo Bohemia", "12", "Benešov", "světlé pivo"));
        beers.add(new BeerLoader("Bohemia Regent speciál", "9", "Regent - Třeboň", "světlé pivo"));
        beers.add(new BeerLoader("Bohemia Regent", "10", "Regent - Třeboň", "světlé pivo"));
        beers.add(new BeerLoader("Bohemia Regent", "12", "Regent - Třeboň", "světlé pivo"));
        beers.add(new BeerLoader("Bohemia Regent", "12", "Regent - Třeboň", "tmavé pivo"));
        beers.add(new BeerLoader("Braník", "10", "Braník", "světlé pivo"));
        beers.add(new BeerLoader("Braník", "12", "Braník", "světlé pivo"));
        beers.add(new BeerLoader("Brouček", "10", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Březňák", "10", "Velké Březno", "světlé pivo"));
        beers.add(new BeerLoader("Březňák", "10", "Velké Březno", "tmavé pivo"));
        beers.add(new BeerLoader("Březňák", "12", "Velké Březno", "světlé pivo"));
        beers.add(new BeerLoader("Budweiser Budvar", "10", "Budějovický Budvar", " pivo"));
        beers.add(new BeerLoader("Budweiser Budvar", "12", "Budějovický Budvar", " pivo"));
        beers.add(new BeerLoader("Budweiser Budvar", "Nealko", "Budějovický Budvar", " pivo"));
        beers.add(new BeerLoader("Budweiser Budvar", "Dia", "Budějovický Budvar", " pivo"));
        beers.add(new BeerLoader("Bud", "17", "Budějovický Budvar", " pivo"));
        beers.add(new BeerLoader("Červený drak", "14", "Starobrno", "červené pivo"));
        beers.add(new BeerLoader("Dačický světlý ležák", "12", "Kutná hora", "světlý ležák"));
        beers.add(new BeerLoader("DIA", "Dia", "Právovárečný pivovar", "DIA pivo"));
        beers.add(new BeerLoader("DIA", "Dia", "Karlovy Vary", "DIA pivo"));
        beers.add(new BeerLoader("DIA", "Dia", "Louny", "světlé pivo"));
        beers.add(new BeerLoader("Drak", "12", "Starobrno", "světlé pivo"));
        beers.add(new BeerLoader("Dvorní ležák", "12", "Pivovarský dvůr Chýně", "karamelové pivo"));
        beers.add(new BeerLoader("Eggenberg", "10", "Eggenberg", "světlé pivo"));
        beers.add(new BeerLoader("Eggenberg", "10", "Eggenberg", "tmavé pivo"));
        beers.add(new BeerLoader("Eggenberg", "12", "Eggenberg", "světlé pivo"));
        beers.add(new BeerLoader("Eggenberg", "Dia", "Eggenberg", "světlé pivo"));
        beers.add(new BeerLoader("Ferdinand", "10", "Benešov", "světlévýčepní pivo"));
        beers.add(new BeerLoader("Ferdinand", "11", "Benešov", "tmavé pivo"));
        beers.add(new BeerLoader("Ferdinand", "12", "Benešov", "světlý ležák pivo"));
        beers.add(new BeerLoader("Ferdinand", "12", "Benešov", "tmavý ležák pivo"));
        beers.add(new BeerLoader("Francinův ležák", "12", "Nymburk", "světlé pivo"));
        beers.add(new BeerLoader("Gambrinus", "8", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Gambrinus", "10", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Gambrinus", "10", "Plzeňský Prazdroj", "tmavé pivo"));
        beers.add(new BeerLoader("Gambrinus", "12", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Gambrinus", "12", "Plzeňský Prazdroj", "tmavé pivo"));
        beers.add(new BeerLoader("Gambrinus bílý", "12", "Plzeňský Prazdroj", "bílépšeničné pivo"));
        beers.add(new BeerLoader("Gambrinus master", "Nealko", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Gambrinus", "Dia", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Gerhard", "10", "Gerhard", "světlé pivo"));
        beers.add(new BeerLoader("Gerhard", "12", "Gerhard", "světlé pivo"));
        beers.add(new BeerLoader("Granát", "12", "Černá Hora", "tmavé pivo"));
        beers.add(new BeerLoader("Granát", "12", "Nová Paka", "tmavé pivo"));
        beers.add(new BeerLoader("Haškův Rebel c.k.", "10", "Rebel", "světlé pivo"));
        beers.add(new BeerLoader("Herold", "10", "Herold Březnice", "světlé pivo"));
        beers.add(new BeerLoader("Herold", "12", "Herold Březnice", "světlé pivo"));
        beers.add(new BeerLoader("Herold", "13", "Herold Březnice", "tmavé pivo"));
        beers.add(new BeerLoader("Hojda", "10", "Pivovarský dvůr Chýně", "světlé pivo"));
        beers.add(new BeerLoader("Hojda", "14", "Pivovarský dvůr Chýně", "tmavé pivo"));
        beers.add(new BeerLoader("Holba", "10", "Holba", "světlé pivo"));
        beers.add(new BeerLoader("Holba (Šerák)", "11", "Holba", "světlé pivo"));
        beers.add(new BeerLoader("Hostan", "10", "Hostan", "světlé pivo"));
        beers.add(new BeerLoader("Hostan", "10", "Hostan", "tmavé pivo"));
        beers.add(new BeerLoader("Hostan", "11", "Hostan", "světlé pivo"));
        beers.add(new BeerLoader("Hostan", "12", "Hostan", "světlé pivo"));
        beers.add(new BeerLoader("Holba", "12", "Holba", "světlé pivo"));
        beers.add(new BeerLoader("Hradební", "10", "Polička", "světlé pivo"));
        beers.add(new BeerLoader("Hradební", "10", "Polička", "tmavé pivo"));
        beers.add(new BeerLoader("Chmelař", "10", "Chmelař - Žatec", "světlé pivo"));
        beers.add(new BeerLoader("Chmelař", "11", "Chmelař - Žatec", "světlé pivo"));
        beers.add(new BeerLoader("Chmelař", "11", "Chmelař - Žatec", "tmavé pivo"));
        beers.add(new BeerLoader("Chmelař", "12", "Chmelař - Žatec", "světlé pivo"));
        beers.add(new BeerLoader("Chodovar", "10", "Chodovar", "světlé pivo"));
        beers.add(new BeerLoader("Chodovar", "10", "Chodovar", "tmavé pivo"));
        beers.add(new BeerLoader("Chodovar", "11", "Chodovar", "světlé pivo"));
        beers.add(new BeerLoader("Chodovar", "11", "Chodovar", "kvasnicové pivo"));
        beers.add(new BeerLoader("Chodovar", "12", "Chodovar", "světlé pivo"));
        beers.add(new BeerLoader("Chodovar", "13", "Chodovar", "světlé pivo"));
        beers.add(new BeerLoader("Janáček", "10", "Janáček - Uherský Brod", "světlé pivo"));
        beers.add(new BeerLoader("Janáček", "10", "Janáček - Uherský Brod", "tmavé pivo"));
        beers.add(new BeerLoader("Janáček", "10", "Janáček - Uherský Brod", "kvasnicové pivo"));
        beers.add(new BeerLoader("Janáček ležák", "12", "Janáček - Uherský Brod", "světlý ležák pivo"));
        beers.add(new BeerLoader("Janáček", "Dia", "Janáček - Uherský Brod", "světlé pivo"));
        beers.add(new BeerLoader("Ječmínek", "Dia", "Prostějov", "světlé pivo"));
        beers.add(new BeerLoader("Ječmínek", "Nealko", "Prostějov", "světlé pivo"));
        beers.add(new BeerLoader("Ječmínek", "10", "Prostějov", "světlé pivo"));
        beers.add(new BeerLoader("Ječmínek", "11", "Prostějov", "světlé pivo"));
        beers.add(new BeerLoader("Ječmínek", "12", "Prostějov", "světlé pivo"));
        beers.add(new BeerLoader("Kaiser premium", "12", "Vysoký Chlumec", "světlé pivo"));
        beers.add(new BeerLoader("Kalich", "14", "Právovárečný pivovar", "světlé pivo"));
        beers.add(new BeerLoader("Kalich", "7", "Litoměřice", "světlé pivo"));
        beers.add(new BeerLoader("Kalich", "10", "Litoměřice", "světlé pivo"));
        beers.add(new BeerLoader("Kalich", "11", "Litoměřice", "světlé pivo"));
        beers.add(new BeerLoader("Kalich", "12", "Litoměřice", "světlé pivo"));
        beers.add(new BeerLoader("Karamelové", "10", "Starobrno", "Karamelové pivo"));
        beers.add(new BeerLoader("Karel", "11", "Karlovy Vary", "světlé pivo"));
        beers.add(new BeerLoader("Kryštof", "11", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Kern", "10", "Černá Hora", "polotmavévýčepní pivo"));
        beers.add(new BeerLoader("Klášter", "8", "Klášter", "světlé pivo"));
        beers.add(new BeerLoader("Klášter", "10", "Klášter", "světlé pivo"));
        beers.add(new BeerLoader("Klášter", "10", "Klášter", "tmavé pivo"));
        beers.add(new BeerLoader("Klášter", "12", "Klášter", "světlé pivo"));
        beers.add(new BeerLoader("Koník", "10", "Ostravar", "světlé pivo"));
        beers.add(new BeerLoader("Korbel", "10", "Korbel - Malý Rohozec", "světlé pivo"));
        beers.add(new BeerLoader("Korbel", "11", "Korbel - Malý Rohozec", "světlé pivo"));
        beers.add(new BeerLoader("Korbel", "11", "Korbel - Malý Rohozec", "tmavé pivo"));
        beers.add(new BeerLoader("Korbel", "12", "Korbel - Malý Rohozec", "světlé pivo"));
        beers.add(new BeerLoader("Královské pivo", "Nealko", "Litovel", "nealko pivo"));
        beers.add(new BeerLoader("Královské pivo", "10", "Litovel", "světlé pivo"));
        beers.add(new BeerLoader("Královské pivo", "10", "Litovel", "tmavé pivo"));
        beers.add(new BeerLoader("Královské pivo", "11", "Litovel", "světlé pivo"));
        beers.add(new BeerLoader("Královské pivo", "12", "Litovel", "světlé pivo"));
        beers.add(new BeerLoader("Královské pivo", "12", "Litovel", "tmavé pivo"));
        beers.add(new BeerLoader("Královský lev", "8", "Právovárečný pivovar", "světlé pivo"));
        beers.add(new BeerLoader("Královský lev", "10", "Právovárečný pivovar", "světlé pivo"));
        beers.add(new BeerLoader("Královský lev", "10", "Právovárečný pivovar", "tmavé pivo"));
        beers.add(new BeerLoader("Královský lev", "12", "Právovárečný pivovar", "světlé pivo"));
        beers.add(new BeerLoader("Krušovice", "10", "Krušovice", "světlé pivo"));
        beers.add(new BeerLoader("Krušovice", "10", "Krušovice", "tmavé pivo"));
        beers.add(new BeerLoader("Krušovice", "12", "Krušovice", "světlé pivo"));
        beers.add(new BeerLoader("Krkonošský medvěd", "10", "Pivovarská bašta - Vrchlabí", "světlé pivo"));
        beers.add(new BeerLoader("Krkonošský medvěd", "12", "Pivovarská bašta - Vrchlabí", "světlé pivo"));
        beers.add(new BeerLoader("Krkonošský medvěd", "12", "Pivovarská bašta - Vrchlabí", "tmavé pivo"));
        beers.add(new BeerLoader("Kumburák", "12", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Kutnohorské", "10", "Kutná hora", "světlývýčepní pivo"));
        beers.add(new BeerLoader("Kutnohorský havíř", "10", "Kutná hora", "tmavé pivo"));
        beers.add(new BeerLoader("Kutnohorské Light", "Dia", "Kutná hora", "světlývýčepní pivo"));
        beers.add(new BeerLoader("Kvasnicový ležák", "10", "Excelent - Rýmařov", "světlékvasnicové pivo"));
        beers.add(new BeerLoader("Kvasnicový ležák", "12", "Excelent - Rýmařov", "světlékvasnicové pivo"));
        beers.add(new BeerLoader("Kvasnicový ležák", "14", "Excelent - Rýmařov", "světlékvasnicové pivo"));
        beers.add(new BeerLoader("Ležák", "12", "Černá Hora", "světlé pivo"));
        beers.add(new BeerLoader("Lobkowicz", "8", "Vysoký Chlumec", "světlé pivo"));
        beers.add(new BeerLoader("Lobkowicz", "10", "Vysoký Chlumec", "světlé pivo"));
        beers.add(new BeerLoader("Lobkowicz", "12", "Vysoký Chlumec", "světlé pivo"));
        beers.add(new BeerLoader("Lobkowicz", "12", "Vysoký Chlumec", "tmavé pivo"));
        beers.add(new BeerLoader("Lobkov", "12", "Vysoký Chlumec", "světlé pivo"));
        beers.add(new BeerLoader("Lobkov", "12", "Vysoký Chlumec", "tmavé pivo"));
        beers.add(new BeerLoader("Lobkov speciál", "14", "Vysoký Chlumec", "speciál pivo"));
        beers.add(new BeerLoader("Lorec", "14", "Kutná hora", "světléspeciální pivo"));
        beers.add(new BeerLoader("Lounská desítka", "10", "Louny", "světlé pivo"));
        beers.add(new BeerLoader("Lounská desítka", "10", "Louny", "tmavá pivo"));
        beers.add(new BeerLoader("Lounský ležák", "12", "Louny", "světlé pivo"));
        beers.add(new BeerLoader("Lounský ležák", "12", "Louny", "tmavé pivo"));
        beers.add(new BeerLoader("Meloun", "12", "U Lípy - Svinišťany", "kvasnicové pivo"));
        beers.add(new BeerLoader("Měšťan", "8", "První pražský měšťanský", "světlé pivo"));
        beers.add(new BeerLoader("Měšťan", "10", "První pražský měšťanský", "světlé pivo"));
        beers.add(new BeerLoader("Měšťan", "11", "První pražský měšťanský", "světlé pivo"));
        beers.add(new BeerLoader("Měšťan", "12", "První pražský měšťanský", "světlé pivo"));
        beers.add(new BeerLoader("Novoměstský ležák", "11", "Novoměstský pivovar", "světlé pivo"));
        beers.add(new BeerLoader("Nektar", "8", "Strakonice", "světlé pivo"));
        beers.add(new BeerLoader("Nektar", "10", "Strakonice", "světlé pivo"));
        beers.add(new BeerLoader("Nektar", "10", "Strakonice", "tmavé pivo"));
        beers.add(new BeerLoader("Nektar", "11", "Strakonice", "světlé pivo"));
        beers.add(new BeerLoader("Old Bohemia Beer", "12", "Nymburk", "světlé pivo"));
        beers.add(new BeerLoader("Ondráš", "12", "Ostravar", "světlý ležák pivo"));
        beers.add(new BeerLoader("Otakar", "11", "Polička", "světlý ležák"));
        beers.add(new BeerLoader("Opat", "10", "Broumov", "světlé pivo"));
        beers.add(new BeerLoader("Opat", "10", "Broumov", "tmavé pivo"));
        beers.add(new BeerLoader("Opat", "11", "Broumov", "světlé pivo"));
        beers.add(new BeerLoader("Opat", "12", "Broumov", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "8", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "9", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "10", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "10", "Pardubice", "polotmavé pivo"));
        beers.add(new BeerLoader("Pernštejn", "11", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "12", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "Nealko", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pernštejn", "Dia", "Pardubice", "světlé pivo"));
        beers.add(new BeerLoader("Pilsner Urquell", "10", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Pilsner Urquell", "12", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Pivrnec", "11", "Sedlec", "světlé pivo"));
        beers.add(new BeerLoader("PITO", "Nealko", "Právovárečný pivovar", "PITO pivo"));
        beers.add(new BeerLoader("Platan", "Dia", "Protovín", "světlé pivo"));
        beers.add(new BeerLoader("Platan FIT", "Nealko", "Protovín", "světlé pivo"));
        beers.add(new BeerLoader("Platan", "10", "Protovín", "světlé pivo"));
        beers.add(new BeerLoader("Platan", "10", "Protovín", "tmavé pivo"));
        beers.add(new BeerLoader("Platan", "11", "Protovín", "světlé pivo"));
        beers.add(new BeerLoader("Platan", "12", "Protovín", "světlé pivo"));
        beers.add(new BeerLoader("Pegas", "12", "Pegas", "černé pivo"));
        beers.add(new BeerLoader("Pegas", "12", "Pegas", "pšeničné pivo"));
        beers.add(new BeerLoader("Pegas", "12", "Pegas", "světlé pivo"));
        beers.add(new BeerLoader("Pelhřimov", "10", "Pelhřimov", "světlé pivo"));
        beers.add(new BeerLoader("Pelhřimov", "12", "Pelhřimov", "světlé pivo"));
        beers.add(new BeerLoader("Pepinova desítka", "10", "Nymburk", "světlé pivo"));
        beers.add(new BeerLoader("Pepinova desítka", "10", "Nymburk", "tmavé pivo"));
        beers.add(new BeerLoader("Podkováňské", "Dia", "Podkováň", "světlé pivo"));
        beers.add(new BeerLoader("Podkováňské", "Nealko", "Podkováň", "světlé pivo"));
        beers.add(new BeerLoader("Podkováňské", "8", "Podkováň", "světlé pivo"));
        beers.add(new BeerLoader("Podkováňské", "10", "Podkováň", "světlé pivo"));
        beers.add(new BeerLoader("Podkováňské", "10", "Podkováň", "tmavé pivo"));
        beers.add(new BeerLoader("Podkováňské", "12", "Podkováň", "světlé pivo"));
        beers.add(new BeerLoader("Portáš", "10", "Vsetín", "světlé pivo"));
        beers.add(new BeerLoader("Portáš", "10", "Vsetín", "tmavé pivo"));
        beers.add(new BeerLoader("Portáš", "11", "Vsetín", "světlé pivo"));
        beers.add(new BeerLoader("Portáš", "12", "Vsetín", "světlé pivo"));
        beers.add(new BeerLoader("Porter", "19", "Pardubice", "tmavé pivo"));
        beers.add(new BeerLoader("Poutník", "12", "Pelhřimov", "světlé pivo"));
        beers.add(new BeerLoader("Primátor", "10", "Náchod", "světlé pivo"));
        beers.add(new BeerLoader("Primátor", "12", "Náchod", "světlé pivo"));
        beers.add(new BeerLoader("Primátor", "12", "Náchod", "tmavé pivo"));
        beers.add(new BeerLoader("Primátor", "15", "Náchod", "polotmavé pivo"));
        beers.add(new BeerLoader("Primátor", "16", "Náchod", "světlé pivo"));
        beers.add(new BeerLoader("Primus", "10", "Karlovy Vary", "světlé pivo"));
        beers.add(new BeerLoader("Primus", "10", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Primus", "12", "Plzeňský Prazdroj", "světlé pivo"));
        beers.add(new BeerLoader("Premium", "12", "Starobrno", "světlé pivo"));
        beers.add(new BeerLoader("Převor", "10", "Broumov", "světlé pivo"));
        beers.add(new BeerLoader("Radegast Triumf", "10", "Nošovice", "světlé pivo"));
        beers.add(new BeerLoader("Radegast", "11", "Nošovice", "světlé pivo"));
        beers.add(new BeerLoader("Radegast Premium", "12", "Nošovice", "světlé pivo"));
        beers.add(new BeerLoader("Radegast Birell", "12", "Nošovice", "světlé pivo"));
        beers.add(new BeerLoader("Radegast", "10", "Sedlec", "světlé pivo"));
        beers.add(new BeerLoader("Radegast", "10", "Sedlec", "tmavé pivo"));
        beers.add(new BeerLoader("Radegast", "18", "Sedlec", "tmavé pivo"));
        beers.add(new BeerLoader("Rallye", "Nealko", "Ostravar", "světlé pivo"));
        beers.add(new BeerLoader("Rebel", "8", "Rebel", "světlé pivo"));
        beers.add(new BeerLoader("Rebel", "10", "Rebel", "světlé pivo"));
        beers.add(new BeerLoader("Rebel", "11", "Rebel", "světlé pivo"));
        beers.add(new BeerLoader("Rebel", "12", "Rebel", "světlé pivo"));
        beers.add(new BeerLoader("Rebel", "12", "Rebel", "tmavé pivo"));
        beers.add(new BeerLoader("Rychtář", "10", "Rychtář", "světlé pivo"));
        beers.add(new BeerLoader("Rychtář", "11", "Rychtář", "světlé pivo"));
        beers.add(new BeerLoader("Rychtář", "12", "Rychtář", "světlé pivo"));
        beers.add(new BeerLoader("Rychtář", "12", "Rychtář", "tmavé pivo"));
        beers.add(new BeerLoader("Rychtář", "10", "Rychtář", "kvasnicové pivo"));
        beers.add(new BeerLoader("Řezák", "12", "Starobrno", "polotmavé pivo"));
        beers.add(new BeerLoader("Samson", "10", "Samson", "světlé pivo"));
        beers.add(new BeerLoader("Samson", "10", "Samson", "tmavé pivo"));
        beers.add(new BeerLoader("Samson", "11", "Samson", "světlé pivo"));
        beers.add(new BeerLoader("Samson", "12", "Samson", "světlé pivo"));
        beers.add(new BeerLoader("Samson", "Dia", "Samson", "světlé pivo"));
        beers.add(new BeerLoader("Samson", "Nealko", "Samson", "světlé pivo"));
        beers.add(new BeerLoader("Slezský korbel", "12", "HSP", " pivo"));
        beers.add(new BeerLoader("Starobrno Osma", "8", "Starobrno", "světlé pivo"));
        beers.add(new BeerLoader("Starobrno Tradiční", "10", "Starobrno", "světlé pivo"));
        beers.add(new BeerLoader("Starobrno Černé", "10", "Starobrno", "černé pivo"));
        beers.add(new BeerLoader("Starobrno Řezák", "10", "Starobrno", "polotmavé pivo"));
        beers.add(new BeerLoader("Starobrno Ležák", "12", "Starobrno", "světlý ležák pivo"));
        beers.add(new BeerLoader("Staročeské výčepní", "10", "Benešov", "světlévýčepní pivo"));
        beers.add(new BeerLoader("Staročeský ležák", "12", "Benešov", "světlý ležák pivo"));
        beers.add(new BeerLoader("Staropramen", "10", "Staropramen", "světlé pivo"));
        beers.add(new BeerLoader("Staropramen", "12", "Staropramen", "světlé pivo"));
        beers.add(new BeerLoader("Strakonický dudák", "8", "Strakonice", "světlé pivo"));
        beers.add(new BeerLoader("Svijany", "8", "Svijany", "světlé pivo"));
        beers.add(new BeerLoader("Svijany", "10", "Svijany", "světlé pivo"));
        beers.add(new BeerLoader("Svitavy", "Dia", "Svitavy", "světlé pivo"));
        beers.add(new BeerLoader("Svitavy", "10", "Svitavy", "světlé pivo"));
        beers.add(new BeerLoader("Svitavy", "10", "Svitavy", "tmavé pivo"));
        beers.add(new BeerLoader("Svitavy", "11", "Svitavy", "světlé pivo"));
        beers.add(new BeerLoader("Svitavy", "12", "Svitavy", "světlé pivo"));
        beers.add(new BeerLoader("Svijanský máz", "11", "Svijany", "světlé pivo"));
        beers.add(new BeerLoader("Svijanský kníže", "12", "Svijany", "světlé pivo"));
        beers.add(new BeerLoader("Světlý ležák", "12", "Starobrno", "světlé pivo"));
        beers.add(new BeerLoader("Světozor", "12", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Švihák", "12", "Kunc", "kvasnicové pivo"));
        beers.add(new BeerLoader("TAS", "10", "Černá Hora", "světlévýčepní pivo"));
        beers.add(new BeerLoader("Tradičnísvětlé", "10", "Starobrno", "světlé pivo"));
        beers.add(new BeerLoader("Valdštejn", "16", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Velkopopovický kozel", "Nealko", "Velké Popovice", "světlé pivo"));
        beers.add(new BeerLoader("Velkopopovický kozel", "10", "Velké Popovice", "světlé pivo"));
        beers.add(new BeerLoader("Velkopopovický kozel", "10", "Velké Popovice", "tmavé pivo"));
        beers.add(new BeerLoader("Velkopopovický kozel", "12", "Velké Popovice", "světlé pivo"));
        beers.add(new BeerLoader("Velkopopovický kozel", "14", "Velké Popovice", "tmavé pivo"));
        beers.add(new BeerLoader("Votrok", "11", "Právovárečný pivovar", "světlé pivo"));
        beers.add(new BeerLoader("Vozkova kvasnicová", "12", "U Vozků - Liberec", "kvasnicové pivo"));
        beers.add(new BeerLoader("Vraník", "10", "Ostravar", "tmavé pivo"));
        beers.add(new BeerLoader("Vratislav", "Nealko", "Vratislavice nad Nisou", "světlé pivo"));
        beers.add(new BeerLoader("Vratislav", "10", "Vratislavice nad Nisou", "světlé pivo"));
        beers.add(new BeerLoader("Vratislav", "11", "Vratislavice nad Nisou", "světlé pivo"));
        beers.add(new BeerLoader("Vratislav", "11", "Vratislavice nad Nisou", "tmavé pivo"));
        beers.add(new BeerLoader("Vratislav Premium", "12", "Vratislavice nad Nisou", "světlé pivo"));
        beers.add(new BeerLoader("Vyškov", "8", "Vyškov", "světlé pivo"));
        beers.add(new BeerLoader("Vyškov", "10", "Vyškov", "světlé pivo"));
        beers.add(new BeerLoader("Vyškov", "11", "Vyškov", "světlé pivo"));
        beers.add(new BeerLoader("Vyškov", "11", "Vyškov", "tmavé pivo"));
        beers.add(new BeerLoader("Vyškov", "12", "Vyškov", "světlé pivo"));
        beers.add(new BeerLoader("Výroční ležák", "13", "Nová Paka", "světlé pivo"));
        beers.add(new BeerLoader("Wallenstein", "11", "Karlovy Vary", "světlé pivo"));
        beers.add(new BeerLoader("Záviš", "12", "Polička", "světlý ležák pivo"));
        beers.add(new BeerLoader("Zlatálabuť", "11", "Zvíkovské Podhradí", "světlý ležák pivo"));
        beers.add(new BeerLoader("Zlatálabuť speciál", "12", "Zvíkovské Podhradí", "světlýspeciál pivo"));
        beers.add(new BeerLoader("Zlatálabuť speciál tmavý", "12", "Zvíkovské Podhradí", "tmavýspeciál pivo"));
        beers.add(new BeerLoader("Zlatník", "10", "Sedlec", "světlé pivo"));
        beers.add(new BeerLoader("Zlatník", "11", "Sedlec", "světlé pivo"));
        beers.add(new BeerLoader("Zlatník", "12", "Sedlec", "světlé pivo"));
        beers.add(new BeerLoader("Zlatopramen", "10", "Ústí nad Labem", "světlé pivo"));
        beers.add(new BeerLoader("Zlatopramen", "11", "Ústí nad Labem", "světlé pivo"));
        beers.add(new BeerLoader("Zlatovar", "11", "Nymburk", "světlé pivo"));
        beers.add(new BeerLoader("Zlatovar", "10", "Zlatovar - Opava", "světlé pivo"));
        beers.add(new BeerLoader("Zlatovar", "12", "Zlatovar - Opava", "světlé pivo"));
        beers.add(new BeerLoader("Zlatýberan", "10", "Knížecí pivovar - Lanškroun", "světlé pivo"));
        beers.add(new BeerLoader("Zlatýberan", "11", "Knížecí pivovar - Lanškroun", "světlé pivo"));
        beers.add(new BeerLoader("Zlatýberan", "12", "Knížecí pivovar - Lanškroun", "světlé pivo"));
        beers.add(new BeerLoader("Zubr", "Nealko", "Zubr", "světlé pivo"));
        beers.add(new BeerLoader("Zubr", "Dia", "Zubr", "světlé pivo"));
        beers.add(new BeerLoader("Zubr", "8", "Zubr", "světlé pivo"));
        beers.add(new BeerLoader("Zubr", "10", "Zubr", "světlé pivo"));
        beers.add(new BeerLoader("Zubr", "10", "Zubr", "tmavé pivo"));
        beers.add(new BeerLoader("Zubr", "12", "Zubr", "světlé pivo"));

        for (BeerLoader beerLoader : beers) {
            Brewery brewery = breweryRepository.findOneByName(beerLoader.getBreweryName());

            if (brewery == null) {
                logger.error("Brewery not found:" + beerLoader.getBreweryName());
                throw new RuntimeException("Brewery not found:" + beerLoader.getBreweryName());
            }

            Beer beer = new Beer();
            beer.setBrewery(brewery);
            beer.setName(beerLoader.getName());
            beer.setDescription(beerLoader.getDescription());
            beer.setDegree(beerLoader.getDegree());
            beer.setAlcohol(beerLoader.getAlcohol());

            Rating rating = new Rating();
            rating.setName("Pavel Pivorád");
            rating.setDate(new Date());
            rating.setDescription("Mě chutná každé české pivko.");
            rating.setRating(4);
            rating.setBeer(beer);

            beer.setRatings(Collections.singletonList(rating));

            beerRepository.save(beer);
        }
    }

    private class BeerLoader {
        private String name;
        private String degree;
        private String breweryName;
        private String description;
        private BigDecimal alcohol;

        public BeerLoader(String name, String degree, String breweryName, String description) {
            this.name = name;
            this.degree = degree;
            this.breweryName = breweryName;
            this.description = description;


            this.alcohol = calculateAlcohol(this.degree);
        }

        private BigDecimal calculateAlcohol(String degree) {

            HashMap<String, BigDecimal> degreeAlcoholMap = new HashMap<String, BigDecimal>();
            degreeAlcoholMap.put("7", new BigDecimal("1.5"));
            degreeAlcoholMap.put("8", new BigDecimal("2"));
            degreeAlcoholMap.put("9", new BigDecimal("2.5"));
            degreeAlcoholMap.put("10", new BigDecimal("3"));
            degreeAlcoholMap.put("11", new BigDecimal("3.5"));
            degreeAlcoholMap.put("12", new BigDecimal("4"));
            degreeAlcoholMap.put("13", new BigDecimal("4.5"));
            degreeAlcoholMap.put("14", new BigDecimal("5"));
            degreeAlcoholMap.put("15", new BigDecimal("5.5"));
            degreeAlcoholMap.put("16", new BigDecimal("6"));
            degreeAlcoholMap.put("17", new BigDecimal("6.5"));
            degreeAlcoholMap.put("18", new BigDecimal("7"));
            degreeAlcoholMap.put("19", new BigDecimal("7.5"));
            degreeAlcoholMap.put("Dia", new BigDecimal("1"));
            degreeAlcoholMap.put("Nealko", new BigDecimal("0"));

            BigDecimal degreeAlcohol = degreeAlcoholMap.get(degree);
            BigDecimal randomizedAlcohol = degreeAlcohol.add(new BigDecimal(Math.random() * 0.5));

            randomizedAlcohol.setScale(1, RoundingMode.CEILING);

            return randomizedAlcohol;
        }


        public BigDecimal getAlcohol() {
            return alcohol;
        }

        public void setAlcohol(BigDecimal alcohol) {
            this.alcohol = alcohol;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDegree() {
            return degree;
        }

        public void setDegree(String degree) {
            this.degree = degree;
        }

        public String getBreweryName() {
            return breweryName;
        }

        public void setBreweryName(String breweryName) {
            this.breweryName = breweryName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
