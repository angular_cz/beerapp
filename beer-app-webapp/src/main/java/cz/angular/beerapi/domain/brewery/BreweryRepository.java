package cz.angular.beerapi.domain.brewery;

/**
 * Created by vita on 15.12.14.
 */

import org.springframework.data.repository.CrudRepository;

public interface BreweryRepository extends CrudRepository<Brewery, Long> {

  Brewery findOneByName(String name);

}
