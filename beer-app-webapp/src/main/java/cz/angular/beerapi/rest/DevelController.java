package cz.angular.beerapi.rest;

import cz.angular.beerapi.security.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by vita on 16.12.14.
 */
@RestController
@RequestMapping(value = "/api")
public class DevelController {

    @Autowired
    TokenAuthenticationService tokenService;

    @Secured("ROLE_USER")
    @RequestMapping(value = "/_devel/secured", method = RequestMethod.GET)
    public Date getSecured() {
        return new Date();
    }

    @Secured("ROLE_DENIED")
    @RequestMapping(value = "/_devel/denied", method = RequestMethod.GET)
    public Date getDenied() {
        return new Date();
    }

    @RequestMapping(value = "/_devel/token-expire", method = RequestMethod.POST)
    public void setTokenExpiration(@RequestBody HashMap<String,String> expires) {
        String expire = expires.get("expires");
        tokenService.setExpires(Integer.valueOf(expire));
    }
}
