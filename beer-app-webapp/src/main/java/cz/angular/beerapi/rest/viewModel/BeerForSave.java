package cz.angular.beerapi.rest.viewModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.angular.beerapi.domain.brewery.Brewery;

import java.math.BigDecimal;

/**
 * Created by vita on 16.12.14.
 */
@JsonIgnoreProperties({"ratings"})
public class BeerForSave {

    private Long id;

    private String name;
    private String description;
    private BigDecimal alcohol;
    private String degree;
    private Long breweryId;

    private Brewery brewery;

    public Long getBreweryId() {

        if (brewery != null) {
            return brewery.getId();
        }

        return breweryId;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public void setBreweryId(Long breweryId) {
        this.breweryId = breweryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public BigDecimal getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(BigDecimal alcohol) {
        this.alcohol = alcohol;
    }
}
