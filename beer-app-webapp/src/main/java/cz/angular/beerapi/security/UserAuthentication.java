package cz.angular.beerapi.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Carries authenticated user
 */
public class UserAuthentication implements Authentication {

	private final UserClaims userClaims;
	private boolean authenticated = true;

	public UserAuthentication(UserClaims userClaims) {
		this.userClaims = userClaims;
	}

	@Override
	public String getName() {
		return userClaims.getUsername();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

		for (String role : userClaims.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role));
		}

		return grantedAuthorities;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public UserClaims getDetails() {
		return userClaims;
	}

	@Override
	public Object getPrincipal() {
		return userClaims.getUsername();
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}
}
