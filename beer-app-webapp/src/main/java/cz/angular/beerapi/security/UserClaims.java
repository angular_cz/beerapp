package cz.angular.beerapi.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* Carries information to be saved in JWT claims
*/
public class UserClaims {
    private Long exp;
    private String username;
    private List<String> roles;

    public UserClaims() {
    }

    public UserClaims(Authentication auth, int seconds) {
        username = auth.getName();
        roles = new ArrayList<String>();
        exp = (new Date().getTime() / 1000) + seconds;

        for (GrantedAuthority authority : auth.getAuthorities()) {
            roles.add(authority.getAuthority());
        }
    }

    public Long getExp() {
        return exp;
    }

    public void setExp(Long exp) {
        this.exp = exp;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
