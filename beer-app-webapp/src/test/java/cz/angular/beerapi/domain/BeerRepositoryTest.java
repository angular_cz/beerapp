package cz.angular.beerapi.domain;

import cz.angular.beerapi.Application;
import org.hibernate.SessionFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;

import static org.junit.Assert.*;

@Ignore("presentation build speed up")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class BeerRepositoryTest {

    @Autowired
    BeerRepository beerRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testSaveBeer() throws Exception {
        Beer beer = new Beer();
        beer.setName("pivko");

        Rating t = new Rating();
        t.setName("pavel");
        t.setBeer(beer);

        beer.setRatings(Collections.singletonList(t));
        beerRepository.save(beer);

        Long id = beer.getId();

        entityManager.clear();
        Beer reloaded = beerRepository.findOne(id);

        assertEquals("pivko", reloaded.getName());
        assertEquals("pavel", reloaded.getRatings().get(0).getName());
    }

    @Test
    public void testAddRating() throws Exception {

        Beer beer = new Beer();
        beer.setName("pivko");

        beerRepository.save(beer);

        Long id = beer.getId();

        entityManager.clear();
        Beer reloaded = beerRepository.findOne(id);

        assertEquals("pivko", reloaded.getName());


        Rating t = new Rating();
        t.setName("pavel");
        t.setBeer(beer);

        reloaded.getRatings().add(t);
        beerRepository.save(reloaded);

         reloaded = beerRepository.findOne(id);


        assertEquals("pavel", reloaded.getRatings().get(0).getName());
    }
}