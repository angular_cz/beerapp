package cz.angular.beerapi.rest;

import cz.angular.beerapi.Application;
import cz.angular.beerapi.domain.brewery.Brewery;
import cz.angular.beerapi.utils.AuthRestTemplateFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Ignore("presentation build speed up")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({"server.port:9999", "logging.level.org.springframework.web: DEBUG"})
public class BreweryTest {

    public static final String LOCALHOST = "http://localhost:9999/api/";

    private RestTemplate template;

    @Before
    public void setUp() throws Exception {
        template = AuthRestTemplateFactory.getTemplateWithUserLogged();
    }

    @Test
    public void cityIsGeocoded() throws Exception {

        Brewery brewery = new Brewery();
        brewery.setName("Pivovar");
        brewery.setYear(1985);
        brewery.setCity("Brno");

        Brewery returnedBrewery = template.postForObject(LOCALHOST + "brewery", brewery, Brewery.class);

        assertEquals("Brno", returnedBrewery.getCity());

        assertNotNull(returnedBrewery.getLatitude());
        assertNotNull(returnedBrewery.getLongitude());
    }
}
